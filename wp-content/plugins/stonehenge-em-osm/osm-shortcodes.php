<?php
/* This file repairs EM shortcodes, so they can be used with this Plugin. */

// Exit if accessed directly
if (!defined('ABSPATH')) exit;


class Stonehenge_EM_OSM_Shortcodes {

	public function __construct() {
		add_filter('em_location_output_placeholder', array( $this, 'replace_location_map_location'), 1, 3);
		add_filter('em_event_output_placeholder', array( $this, 'replace_location_map_event'), 1, 3);

		remove_shortcode('locations_map');
		add_shortcode('locations_map', array($this, 'em_get_locations_map_shortcode'));

		remove_shortcode('locations-map');
		add_shortcode('locations-map', array($this, 'em_get_locations_map_shortcode'));

		remove_shortcode('events_map');
		add_shortcode('events_map', array($this, 'em_get_events_map_shortcode'));

		remove_shortcode('location');
		add_shortcode('location', array($this, 'em_get_location_shortcode'));

		remove_shortcode('event');
		add_shortcode('event', array($this, 'em_get_event_shortcode'));

	}

	#===============================================
	# 	#_LOCATIONMAP in EM Location Object.
	#===============================================
	function replace_location_map_location( $replacement, $EM_Location, $result ) {
		if( $result === '#_LOCATIONMAP' ) {
			$id = $EM_Location->location_id;
			$replacement = Stonehenge_EM_OSM::construct_single_location_map($id);
		}
		return $replacement;
	}


	#===============================================
	# 	#_LOCATIONMAP in EM Event Object.
	#===============================================
	function replace_location_map_event( $replacement, $EM_Event, $result ) {
		if( $result === '#_LOCATIONMAP' ) {
			if($EM_Event->location_id) {
				$replacement = Stonehenge_EM_OSM::construct_single_location_map($EM_Event->location_id);
			}
			else {
				$replacement = __('This event does not have a location selected.', 'stonehenge-em-osm');
			}
		}
		return $replacement;
	}


	#===============================================
	# 	[locations_map]
	#===============================================
	public static function em_get_locations_map_shortcode( $args ) {
		return Stonehenge_EM_OSM::construct_multiple_map( $args );
	}


	#===============================================
	# 	[events_map]
	#===============================================
	public static function em_get_events_map_shortcode( $args ) {
		return Stonehenge_EM_OSM::construct_multiple_map( $args );
	}


	#===============================================
	# 	[location]
	#===============================================
	function em_get_location_shortcode($atts, $format='') {
		global $EM_Location;
		$atts = (array) $atts;
		$atts['format'] = ($format != '' || empty($atts['format'])) ? $format : $atts['format'];
		$atts['format'] = html_entity_decode($atts['format']); //shorcode doesn't accept html
		if( !empty($atts['location']) && is_numeric($atts['location']) ){
			$EM_Location = em_get_location($atts['location']);
			return ( !empty($atts['format']) ) ? $EM_Location->output($atts['format']) : $EM_Location->output_single();
		}elseif( !empty($atts['post_id']) && is_numeric($atts['post_id']) ){
			$EM_Location = em_get_location($atts['post_id'],'post_id');
			return ( !empty($atts['format']) ) ? $EM_Location->output($atts['format']) : $EM_Location->output_single();
		}
		//no specific location or post id supplied, check globals
		global $EM_Location, $post;
		if( !empty($EM_Location) ){
			return ( !empty($atts['format']) ) ? $EM_Location->output($atts['format']) : $EM_Location->output_single();
		}elseif( $post->post_type == EM_POST_TYPE_LOCATION ){
			$EM_Location = em_get_location($post->ID,'post_id');
			return ( !empty($atts['format']) ) ? $EM_Location->output($atts['format']) : $EM_Location->output_single();
		}
	}


	#===============================================
	# 	[event]
	#===============================================
	function em_get_event_shortcode($atts, $format='') {
	    global $post, $EM_Event;
		$return = '';
		$atts = (array) $atts;
		$atts['format'] = ($format != '' || empty($atts['format'])) ? $format : $atts['format'];
		$atts['format'] = html_entity_decode($atts['format']); //shorcode doesn't accept html
		if( !empty($atts['event']) && is_numeric($atts['event']) ){
			$EM_Event = em_get_event($atts['event']);
			$return = ( !empty($atts['format']) ) ? $EM_Event->output($atts['format']) : $EM_Event->output_single();
		}elseif( !empty($atts['post_id']) && is_numeric($atts['post_id']) ){
			$EM_Event = em_get_event($atts['post_id'], 'post_id');
			$return = ( !empty($atts['format']) ) ? $EM_Event->output($atts['format']) : $EM_Event->output_single();
		}
		//no specific event or post id supplied, check globals
		if( !empty($EM_Event) ){
		    $return = ( !empty($atts['format']) ) ? $EM_Event->output($atts['format']) : $EM_Event->output_single();
		}elseif( $post->post_type == EM_POST_TYPE_EVENT ){
		    $EM_Event = em_get_event($post->ID, 'post_id');
		    $return = ( !empty($atts['format']) ) ? $EM_Event->output($atts['format']) : $EM_Event->output_single();
		}
	    return $return;
	}




} // End class.

