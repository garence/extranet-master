<?php
/**
 * Page : Établissements par pôle
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package clementine
 */

get_header();
$adresse = get_field( "adresse" );
$zipcode = get_field( "code_postal" );
$city = get_field( "ville" );
$telephone = get_field( "numero_de_telephone_1" );
$mail = get_field( "adresse_mail" );
$pole = get_field_object('field_5c92457756909');
$pole2 = $pole['value'][0];

?>

<div class="row col-xl-12">
  	<div class="col-xl-12">
    	<div class="ct-container ct-directory col-xl-12">
      		<div class="ct-title">
        		<h2>Établissements du pôle <?php echo $pole2->name; ?></h2>
		        <div class="searchbar-ets">
			        <?php echo do_shortcode('[searchform]'); ?>
		        </div>
				<?php include "breadcrumb.php"; ?>
      		</div>
      		<br>
      		<div class="row col-xl-12 justify-content-between">
				<section id="primary" class="content-area">
					<main id="main" class="site-main">
						<div class="scrollable-table">
							<table class="tftable gfg order-table" border="0" id="table-id">
								<thead class="tr-head">
									<th>Nom</th>
									<th>Adresse</th>
									<th>Ville</th>
									<th>Mail</th>
									<th>Téléphone</th>
									<th>Pôle</th>
								</thead>

								<?php if ( have_posts() ) : ?>
								<?php while ( have_posts() ) :				
									the_post();
									$i++;
								?>

								<tr class="<?php echo $pole2->slug; ?>">
									<td class="an-name <?php echo $pole2->slug; ?>"><b><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></b>
									</td>
									<td class="an-adress"><?php echo $adresse; ?></td>
									<td class="an-city"><?php echo $city; ?></td>
									<td class="an-mail"><?php echo $mail; ?></td>
									<td class="an-phone"><?php echo $telephone; ?></td>
									<td class="an-pole"><?php echo $pole2->name; ?></td>
								</tr>

								<?php endwhile;
									else :
										get_template_part( 'template-parts/content', 'none' );
									endif;
								?>

							</table>
						</div>
					</main>
				</section>
    		</div>  
  		</div>
  	</div>
</div>

<?php
get_sidebar();
get_footer();
