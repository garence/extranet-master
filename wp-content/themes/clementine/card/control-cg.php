<?php include 'template/header-control-card.php';

    /* Template Name: Liste card Champ goubert à traiter */

        $args = array(
            'post_type' => 'card_champ_goubert',
            'posts_per_page' => 10,
            'post_status' => array( 'publish', 'pending'),
        );

        $query = new WP_Query( $args );
             
        if ( $query->have_posts() ) {
             
            while ( $query->have_posts() ) {
             
                $query->the_post();
                $photo = get_field('field_5c5aef2f11089');
                $checked = get_field('field_5c5aef2f110a5');

                    if ($checked == "Non") {

                        $status="no";
                        $status_text="À valider";

                    } else {

                        $status="yes";
                        $status_text="Validé";

                    }
                    

                include 'template/content-control-card.php'; 
         
            }
             
        } else { ?>

              <span class="alert alert-danger">Cette section est vide !</span>

              <?php
          }
             
            wp_reset_postdata();
            get_footer();
            include 'template/footer-control-card.php';

?>