<?php /*Template Name: Formulaire carte professionnelle SATC*/;?>
<?php acf_form_head(); ?>
<?php get_header(); ?>

<div id="container" class="col-xl-12 ask-form" >
	<div class="ct-container">
	    <div class="row">
	    	<div class="col-sm-12">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				
				<div class="ct-title">
				<h2><?php the_title(); ?></h2>
   	     			<div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
				</div>
				<?php the_content(); ?>
				<!------------>
		
				<?php  $options = array(
					'post_id'				=> 'new_satc',
					'field_groups' 			=> array(852),
					'post_title'			=> false,
					'post_type'				=> 'post',
					'post_status'			=> 'draft',
			        'return' => '?my_param=true&updated=true',
					'submit_value'			=> 'Envoyer ma demande',
					'html_updated_message'	=> '<div class="alert alert-success">Votre demande a été traité avec succès ! Elle est désormais soumise à validation auprès de la direction. Une fois réalisée, la direction reviendra vers vous afin de vous transmettre votre carte professionnelle.</div>',
				);
				 acf_form($options);
				 ?>

			<?php endwhile; ?>
			</div>
		</div><!-- #content -->
	</div><!-- #primary -->
</div>

<?php get_footer(); ?>