<?php
/**
 * Page : Actualités
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 * T
 */

get_header();
$categories     =     get_the_category();
$category_name  =     $categories[0]->name;

?>
<div class="col-xl-12">
    <div class="row justify-content-md-center">
        <div class="ct-container ct-featured list-item col-xl-10">
            <?php 
            include 'breadcrumb.php';
            ?>
            <?php echo do_shortcode('[ajax_load_more id="7921847285" posts_per_page="5" container_type="div" repeater="template_3" post_type="post" category="actualites"]'); ?>
    </div>
</div>
<style>
    .entry-img{
      background:             linear-gradient(90deg, rgba(0, 0, 0, 0.50), rgba(0, 0, 0, 0.50)), url('<?php echo get_the_post_thumbnail_url(); ?>');
      background-size:        cover;
      background-position:    center;
    }
</style>
<?php
get_sidebar();
get_footer();