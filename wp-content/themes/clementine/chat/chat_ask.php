<?php /*Template Name: Page : Ask Chat*/;?>
<?php get_header(); ?>

<div class="col-xl-12">
	<div class="ct-container">
		<form name="ets-form" id="ets-form" method="POST" action="" onchange="changeAction(this)">
			<div class="ct-title">
				<h2>
					<label for="ets-select">Choix du type de chat</label>
				</h2>
		        <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 	
			</div>
							<p>Un chat général est ouvert à tous les membres de l'association. Un chat spécifique est accessible à un groupe de personnes seulement.</p>
					<div class="clm-select">
				<select name="choose-ets" id="ets-select" class="minimal">
				    <option value="">--Choisir--</option>

				    <option value="general/">Chat général</option>
				    <option value="specific/">Chat spécifique</option>
		
				</select>
			</div>
			<input class="btn padding10-20" type="submit" value="Continuer">
		</form>	
	</div>
</div>

<script type="text/javascript">
changeAction = function(select){
   document.getElementById("ets-form").action = document.getElementById("ets-select").value;
}
</script>

<?php 
get_footer(); ?>