<?php 

/* Template Name: Page : Accueil Chat */

get_header();

$title = get_the_title();

?>

<div class="row col-xl-12">
  <div class="col-xl-3 left-col">
    <div class="ct-container col-xl-12 ct-channel eq-padding">
        <a class="btn width-100 padding10-20" href="http://localhost:8888/extranet/chat/add/">
          Créer un chat
        </a>
    </div>
    <div class="ct-container col-xl-12 ct-channel">
      <h2 class="ct-title">Chats généraux</h2>
       <ul>
      <?php 




// 1. on défini ce que l'on veut
$args = array(
    'post_type' => 'channel',
    'posts_per_page' => 5,
    'tax_query' => array(
    array(
      'taxonomy' => 'status',
      'field'    => 'slug',
      'terms'    => 'public',
    ),
  ),
);

// Custom query.
$query = new WP_Query( $args );
 
// Check that we have query results.
if ( $query->have_posts() ) {
 
    // Start looping over the query results.
    while ( $query->have_posts() ) {
 
        $query->the_post();
?>
 
        <li>
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?></a>
        </li>
 
        <?php
 
    }
 
}
 
// Restore original post data.
wp_reset_postdata();
 
?>
  </ul>
    </div>
    <div class="ct-container col-xl-12">
      <h2 class="ct-title">Chats spécifiques</h2>
        <ul>
          <?php 
            $args = array(
              'post_type' => 'channel',
              'posts_per_page' => 5,
              'tax_query' => array(
                array(
                  'taxonomy' => 'status',
                  'field'    => 'slug',
                  'terms'    => 'prive',
                ),
              ),
            );
            $query = new WP_Query( $args ); 
            if ( $query->have_posts() ) {
              while ( $query->have_posts() ) {
                $query->the_post();
          ?>
          <li>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
              <?php the_title(); ?>  
            </a>
          </li>
          <?php
              } 
            } else {
              echo "Vous n'êtes inscrit à aucun chat privé.";
              }
              wp_reset_postdata();
          ?>
      </ul>
    </div> 
  </div>

  <div class="col-xl-9">
    <div class="ct-container ct-chat col-xl-12">
	<div class="row col-xl-12">
    <div class="ct-title">
      <h2><?php the_title(); ?></h2>
      <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
    </div>
		<br>
		<?php
		while ( have_posts() ) :
			the_post();

			the_content();
			// If comments are open or we have at least one comment, load up the comment template.
			
		endwhile; // End of the loop.
		?>
	</div>
    </div>  
  </div>

</div>

<?php
get_footer();

?>