<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 * Template Name: Formation */

get_header();
?>

<div class="row col-xl-12">
    <div class="col-xl-3">
        <?php 
            $roles=$current_user->roles;
            if (in_array("rh", $roles) or (in_array("administrator", $roles))){  
        ?>
        <div class="ct-container col-xl-12 ct-inner-sidebar eq-padding">
            <a href="http://localhost:8888/extranet/rh/formation/add/" class="btn width-100 padding10-20">Ajouter une formation</a>                    
        </div>
        <?php 
              }
        ?>
        <div class="ct-container ct-subpages col-xl-12">
            <h2 class="ct-title">Raccourcis</h2>
            <?php echo do_shortcode('[subpages depth="-1" childof="571"]'); ?>
        </div>    
    </div>
    <div class="col-xl-9">
        <div class="ct-container ct-formation col-xl-12">
            <?php include 'template-parts/header_page.php'; ?>
            <div class="row col-xl-12 justify-content-between">
      	        <div id="primary" class="content-area">
		                <main id="main" class="site-main">
                    		<?php
                    		while ( have_posts() ) :
                            the_post();
                    			  get_template_part( 'template-parts/content', get_post_type() );
                    		endwhile; // End of the loop.
                    		?>
		                </main>
	              </div>
            </div>
        </div>  
    </div>
</div>
<?php
get_sidebar();
get_footer();
