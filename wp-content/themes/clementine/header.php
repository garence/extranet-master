<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package clementine
 */

	global $current_user; 
	wp_get_current_user();
	get_currentuserinfo();
	redirect_guest();
	$mail_user		=		$current_user->user_email;
	$name_user		=		$current_user->user_firstname." ".$current_user->user_lastname; 
	$my_id			=		get_current_user_id();
	$my_meta		=		get_user_meta( $my_id, '', false );
	$nb_login		=		$my_meta["login_amount"][0];

	// if ($nb_login=="1") {
	// 	fill_identity();
	// }
 ?>

<input type="hidden" id="myMail" value="<?php echo $mail_user ?>" /> 
<?php
	if ($_COOKIE['iwashere'] != "yes" && $nb_login=="1") { 
	  setcookie("iwashere", "yes", time()+315360000);  
	}
?>
<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,400,500,600,700,800,900" rel="stylesheet">
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<div id="sidebar-close" class="close"></div>
		<?php get_sidebar(); ?>
		<div class="pusher">
			<div class="hidder2"></div>
			<div class="hidder"></div>
			<nav class="hd-menu">
				<?php
					wp_nav_menu( array(
						'theme_location' 	=> 		'menu-1',
						'menu_id'        	=> 		'primary-menu',
					) );
				?>
			</nav>
			<div id="page">
				<header class="container-fluid row">
					<div class="col-xl-2 col-lg-3 current-user-avatar">
						<?php
					       global $current_user;
					       get_currentuserinfo();     
					       echo get_avatar( $current_user->ID, 64 );
						?>
						<a href="<?php echo get_home_url(); ?>/account">
							<div class="avatar-onhover">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/edit.png" alt="Modifier mon profil" class="edit-profile">
							</div>
						</a>
					</div>
					<div class="hd-left col-xl-8 col-lg-6 col-sm-12">
						<h1>
							<a href="<?php echo get_home_url(); ?>"><?php bloginfo( 'name' ); ?></a>
						</h1>
						<p>Bienvenue <span class="name"><?php echo $current_user->user_firstname." ".$current_user->user_lastname ; ?></span></p>
						<span class="btn">
							<a href="http://www.acsea.asso.fr" target="_blank">Retour au site</a>
						</span>
						<span class="btn btn-danger">
							<a href="<?php echo wp_logout_url(); ?>">Se déconnecter</a>
						</span>
					</div>
					<div class="is-mobile">
						<div></div>
						<div></div>
						<div></div>
					</div>
					<div class="hd-right col-xl-2 col-lg-2 offset-xl-0 offset-lg-1 col-sm-12">
						<div id="sidebar-trigger" class="hd-sidebar sidebar-btn"></div>
						<img class="hd-logo" src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logo.png" alt="Logo" />
					</div>
				</header>
			<!--Youtube like progressbar code start-->
			<script>
				jQuery(document).ready(function() {
				jQuery("body").append(jQuery("<div><dt/><dd/></div>").attr("id", "progress"));
				jQuery("#progress").width(100+ "%");
				jQuery("#progress").width("101%").delay(800).fadeOut(400, function() {
				jQuery(this).remove();
				});
				});
			</script>
			<!--Youtube like progressbar code end-->
			<div id="content" class="container-fluid-2 content row">