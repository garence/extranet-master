<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package clementine
 * Template Name: Avantages et services
 
 */

    get_header();
    $date     =     get_the_date();
    $time     =     get_the_time();

?>

<div class="row col-xl-12">
    <div class="col-xl-3">
        <?php 
             $roles       =       $current_user->roles;
             if (in_array("cce", $roles) or (in_array("administrator", $roles))){  
        ?>
        <div class="ct-container col-xl-12 ct-inner-sidebar eq-padding">
            <a href="add" class="btn width-100 padding10-20">Ajouter un avantage/service</a>                    
        </div>
        <?php 
                }
        ?>
        <div class="ct-container col-xl-12 ct-inner-sidebar">
            <h2 class="ct-title">CCE</h2>
            <?php echo do_shortcode('[subpages depth="-1" childof="1248"]'); ?>
        </div>
    </div>
    <div class="col-xl-9">
        <div class="breadcrumb ct-container eq-padding"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
        <div class="ct-container ct-avantages col-xl-12">
            <div class="ct-title">
                <h2>Les avantages</h2>
            </div>
            <div class="row col-xl-12 justify-content-between">
                <?php
                    $paged    =    ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
                    $args     =    array(

                        'post_type' => 'avantages_services',
                        'posts_per_page' => 20

                    );

                    $query = new WP_Query( $args );
                     if ( $query->have_posts() ) {
                       
                          while ( $query->have_posts() ) {
                       
                              $query          ->     the_post();
                              $lien           =      get_field ('field_5d15dc5cc144d');
                              $description    =      get_field ('field_5d15dc54c144c');
                              $genre          =      get_field('field_5d161594ef9e7');
                ?>

              <article id="post-<?php the_ID(); ?>" class="is-<?php echo $genre; ?> col-xl-6 col-lg-6">
     
                  <div class="box-meta d-flex align-items-center justify-content-center">
                      <h3 class="meta-title">
                          <?php the_title(); ?>
                      </h3>
                      <div class="meta-description"><?php echo $description ?></div>
                      <a href="<?php echo $lien ?>"><span class="meta-link btn padding10-20">Voir</span></a>

                      <?php 
                            $roles    =    $current_user->roles;
                            if (in_array("cce", $roles) or (in_array("administrator", $roles))){  
                              echo delete_post(); 
                            }
                      ?>

                  </div>
              </article>
              <?php
                        }
                     
                    } else { 

              ?>     
              <span class="alert alert-danger">Cette section est vide !</span>
              <?php
                  }
                      next_posts_link( 'Older Entries', $query->max_num_pages );
                      previous_posts_link( 'Next Entries &raquo;' ); 
                      wp_reset_postdata();
              ?>
        </div>
    </div>
    <div class="ct-container ct-services col-xl-12">
        <div class="ct-title">
            <h2>Les services</h2>
        </div>
        <div class="row col-xl-12 justify-content-between">
            <?php
                $paged    =   ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
                $args     =   array(

                    'post_type'       =>      'avantages_services',
                    'posts_per_page'  =>      20

                );

                $query    =    new WP_Query( $args );
                if ( $query->have_posts() ) {
                   
                      while ( $query->have_posts() ) {
                   
                          $query          ->    the_post();
                          $lien           =     get_field ('field_5d15dc5cc144d');
                          $description    =     get_field ('field_5d15dc54c144c');
                          $genre          =     get_field('field_5d161594ef9e7');

            ?>
            <article id="post-<?php the_ID(); ?>" class="is-<?php echo $genre; ?> col-xl-6 col-lg-6">
                <div class="box-meta d-flex align-items-center justify-content-center">
                    <h3 class="meta-title">
                        <?php the_title(); ?>
                    </h3>
                    <div class="meta-description"><?php echo $description ?></div>
                    <a href="<?php echo $lien ?>"><span class="meta-link btn padding10-20">Voir</span></a>

                    <?php 
                          $roles      =       $current_user->roles;
                          if (in_array("cce", $roles) or (in_array("administrator", $roles))){  
                            echo delete_post(); 
                          }
                    ?>

              </div>
          </article>
          <?php
                    }
                } else{ 
          ?>
          <span class="alert alert-danger">Cette section est vide !</span>
          <?php
                }
                  next_posts_link( 'Older Entries', $query->max_num_pages );
                  previous_posts_link( 'Next Entries &raquo;' ); 
                  wp_reset_postdata();
          ?>
      </div>
  </div>    
  </div> 

  <?php
      get_sidebar();
      get_footer();
