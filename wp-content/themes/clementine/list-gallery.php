<?php 
/* Template Name: Médias - liste des galeries */
get_header(); ?>
<div class="row col-xl-12">
    <div class="col-xl-3">
        <div class="ct-container col-xl-12 ct-inner-sidebar eq-padding ">
            <a href="add" class="btn padding10-20 no-mt">Ajouter un album</a>
        </div>
        <div class="ct-container col-xl-12 ct-inner-sidebar">
            <h2 class="ct-title">Trier par établissement</h2>
            <?php 

                if ( have_posts() ) : while ( have_posts() ) : the_post();
                $args     =     array('hide_empty' => true);
                $terms    =     get_terms( 'ets', $args );

                foreach ( $terms as $term ) {

                    // The $term is an object, so we don't need to specify the $taxonomy.
                    $term_link = get_term_link( $term );
                    // If there was an error, continue to the next term.
                    if ( is_wp_error( $term_link ) ) { continue; }
                    // We successfully got a link. Print it out.
                    echo '<div><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';
                    echo  term_description($post->ID,$term).'</div>';
                  }

                endwhile; else : 
            ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
        </div>    
    </div>
    <div class="col-xl-9">
        <div class="ct-container ct-gallery col-xl-12">
            <?php include "template-parts/header_page.php"; ?>
            <br>                
                    <?php echo do_shortcode('[ajax_load_more id="1545529894" container_type="div" repeater="template_1" post_type="gallery" posts_per_page="6" transition_container_classes="row col-xl-12"]'); ?>
          <?php
            get_footer(); 
          ?>
        </div>  
    </div>
</div>
<?php get_sidebar(); ?>