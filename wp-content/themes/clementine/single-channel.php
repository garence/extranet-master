<?php 
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 */

get_header();
$title = get_the_title();
?>
<div class="row col-xl-12 ct-channel">
    <div class="col-xl-3">
        <div class="ct-container col-xl-12 ct-channel eq-padding">
            <a class="btn width-100 padding10-20" href="http://localhost:8888/extranet/chat/add/">Créer un chat</a>
        </div>
        <div class="ct-container col-xl-12">
            <h2 class="ct-title">Salons généraux</h2>
            <ul>
                <?php 
                    $args = array(
                        'post_type'       =>    'channel',
                        'posts_per_page'  =>    5,
                        'tax_query'       =>    array(
                        array(
                            'taxonomy' =>     'status',
                            'field'    =>     'slug',
                            'terms'    =>     'public',
                        ),
                      ),
                    );
                    $query  =   new WP_Query( $args ); 
                    if ( $query->have_posts() ) {
                        while ( $query->have_posts() ) {
                            $query->the_post();
                ?>
                <li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php the_title(); ?></a></li>
                <?php
                  }
                }
                  wp_reset_postdata();
                ?>
            </ul>
        </div>
        <div class="ct-container col-xl-12">
            <h2 class="ct-title">Salons privés</h2>
            <ul>
                <?php 
                  $args = array(
                    'post_type'       =>    'channel',
                    'posts_per_page'  =>    5,
                    'tax_query'       =>    array(
                      array(
                          'taxonomy'    =>    'status',
                          'field'       =>    'slug',
                          'terms'       =>    'prive',
                      ),
                    ),
                  );
                  $query = new WP_Query( $args ); 
                  if ( $query->have_posts() ) {
                      while ( $query->have_posts() ) {
                          $query->the_post();
                ?>
              <li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                  <?php the_title(); ?></a></li>
              <?php
                  } 
                } else {
                  echo "Vous n'êtes inscrit à aucun chat privé.";
                  }
                  wp_reset_postdata();
              ?>
            </ul>
        </div> 
    </div>
    <div class="col-xl-9">
        <div class="ct-container ct-chat col-xl-12">
            <?php include "breadcrumb.php"; ?>
            <div class="row col-xl-12 justify-content-between inner-chat">
      	        <div id="primary" class="content-area">
		                <main id="main" class="site-main"><?php echo do_shortcode("[wise-chat channel= '$title' ]"); ?></main>
	              </div>
            </div>
        </div>  
    </div>
</div>
<?php 
get_sidebar();
get_footer();
