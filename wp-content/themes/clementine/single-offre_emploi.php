<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 */
acf_form_head();
get_header();
$poste              =     get_field( "field_5cecf61e8ddfc" );
$message            =     get_field( "field_5cecee0e97eb6" );
$date_publication   =     get_field( "field_5c77a0dc1faf8" );
$date_echeance      =     get_field( "field_5c77a0ed1faf9" );
$lieu               =     get_field( "field_5c77a1861fafe" );
$contrat            =     get_field( "field_5c77a10c1fafa" );
$ets                =     get_field( "field_5c779f631faf6" );
$contact            =     get_field( "field_5c77a1b01faff" );
$address            =     get_field( "field_5c77a1bf1fb00" );
$volume_horaire     =     get_field("field_5c77a13f1fafb");
$duration           =     get_field("field_5c77a1531fafc");
$temps              =     get_field("field_5c77a17b1fafd");
?>
<div class="row col-xl-12">
    <div class="col-xl-3">
        <div class="ct-container ct-more-info col-xl-12">
            <div class="ct-title">
                <h2>Informations</h2>
            </div>
            <div class="ct-meta-oe">
                <ul>
                    <li>
                        <b>Date de publication de l'annonce </b>
                        <p><?php echo $date_publication; ?></p>
                    </li>
                    <li>
                        <b>Date d'échéance de l'annonce </b>
                        <p><?php echo $date_echeance; ?></p>
                    </li>
                    <li>
                        <b>Lieu</b>
                        <p><?php echo $lieu; ?></p>
                    </li>
                    <li>
                        <b>Établissement</b>
                        <p><?php echo $ets; ?></p>
                    </li>
                    <li>
                        <b>Type de contrat</b>
                        <p><?php echo $contrat; ?></p>
                    </li>
                    <?php if( get_field('field_5c77a1531fafc') ): ?>
                    <li>
                        <b>Durée du poste</b>
                        <p><?php echo $duration; ?></p>
                    </li>
                    <?php endif; ?>
                    <li>
                        <b>Volume horaire</b>
                        <p><?php echo $volume_horaire; ?></p>
                    </li>
                    <?php if( get_field('field_5c77a17b1fafd') ): ?>
                    <li>
                        <b>Temps</b>
                        <p><?php echo $duration; ?></p>
                    </li>
                    <?php endif; ?>
                    <li>
                        <b>Pour faire acte de candidature, contactez : </b>
                        <p><?php echo $contact; ?></p>
                        <p><?php echo $address; ?></p>
                    </li>
                </ul>
            </div>
            <div class="ct-postulate">
                <div class="ct-title">
                    <h2>Postuler</h2>
                </div>
                <input class="btn" type="submit" value="Postuler à l'annonce" data-toggle="modal" data-target="#exampleModalLong">
            </div>
        </div>    
    </div>
    <div class="col-xl-9">
        <div class="ct-container ct-offer col-xl-12">
            <div class="ct-title">
                <h2><?php echo $poste ?></h2>
                <?php include "breadcrumb.php"; ?>
            </div>
            <div class="row col-xl-12 justify-content-between content-offer">
      	        <div id="primary" class="content-area">
		                <main id="main" class="site-main">
		                    <?php 
                            echo      $message;
                            $roles  = $current_user->roles;
                            if (in_array("ressources-humaines", $roles) or (in_array("administrator", $roles))){  echo delete_post(); echo editer_post();
                             }
                        ?>
		                </main>
	              </div>
            </div>
            <div class="offer-edit-box">
                <?php acf_form(array(
                    'post_id'               =>              $post->ID,
                    'post_title'            =>              false,
                    'submit_value'          =>              'Mettre à jour',
                    'html_updated_message'  =>              '<div class="alert alert-success" role="alert"><p>Offre mise à jour avec succès !</p></div>',
                )); ?>
            </div>
        </div>  
    </div>
</div>
<?php
get_sidebar();
get_footer();
