<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 */
get_header();
?>
<div class="ct-container ct-directory col-xl-12 ">
	<div id="primary" class="content-area col-xl-12 col-md-12">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content-actu', get_post_type() );
			the_post_navigation();
		endwhile; 
		?>
		</main>
	</div>
</div>
<style>
	.entry-img{
		background:             linear-gradient(90deg, rgba(0, 0, 0, 0.50), rgba(0, 0, 0, 0.50)), url('<?php echo get_the_post_thumbnail_url(); ?>');
		background-size: 		cover;
		background-position: 	center;
	}
</style>
<?php
get_sidebar();
get_footer();
