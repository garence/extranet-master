<?php 

	$terms        =     get_terms( 'ets', $args );
	$term         =     get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
	$term_name    =     $term->name;
	$term_slug    =     $term->slug;
	$cpt          =     get_post_type( $post->ID );
	$tax_name     =     get_query_var( 'taxonomy' ); 

?>