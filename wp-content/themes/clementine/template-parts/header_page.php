<?php
	  $description = get_field("field_5da6ceed08344");
?>
    <div class="ct-title">
    	<h2><?php the_title(); ?></h2>
        <span><?php echo $description ?></span>
        <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
    </div>