<!-- Bienvenue -->
<div class="modal hide fade tutorial" id="bienvenue1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Bienvenue sur l'extranet associatif de l'ACSEA !</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <div class="col-xl-3 left">
                    <img src="../extranet/wp-content/uploads/2019/08/logo_couleur-e1565770877506.png" class="modal-img" alt="">
                </div>
                <div class="col-xl-8 right">
                    <p>Ce nouvel espace a pour objectif de faciliter votre utilisation des ressources associatives. Il comporte un certain nombre de contenus et de fonctionnalités que nous vous proposons de découvrir dans un court tutoriel en cliquant sur <i class="orange">« Voir le tutoriel ».</i></p>
                    <p>Vous pouvez passer cette étape et utiliser directement l’extranet en cliquant sur <i class="orange">« Passer »</i></p>
                    <p>Une présentation de l'association est également disponible en cliquant sur le bouton <i class="orange">« Accéder à l'accueil nouveau salarié »</i></p>
                    <p>Cette version étant la première, nous vous serions reconnaissant de signaler les éventuels bugs rencontrés lors de votre utilisation du service. Nous sommes aussi preneur de vos suggestions pour l’amélioration de la plateforme !</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary is-grey" data-dismiss="modal">Passer</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainHome" data-dismiss="modal">Voir le tutoriel</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainHome" data-dismiss="modal">Accéder à l'accueil nouveau salarié</button>
            </div>
        </div>
    </div>
</div>
<!-- Accueil -->
<div class="modal hide fade tutorial" id="explainHome" tabindex="-1" role="dialog" aria-labelledby="explainHome" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/home.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'accueil et ses raccourcis</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>Votre espace d’accueil regroupe différents raccourcis vers les fonctionnalités phares de la plateforme. </p>
                <p>Ils permettent d'avoir accès rapidement à l'essentiel du contenu récent et des fonctionnalités fréquemment utilisées.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainNavigation" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Menu de navigation -->
<div class="modal hide fade tutorial" id="explainNavigation" tabindex="-1" role="dialog" aria-labelledby="explainHome" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/nav.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">La navigation</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>Située à gauche de votre écran, elle vous permet d’accéder à tout moment à l’intégralité des contenus de l’extranet. Les fonctionnalités s’affichent au survol de la souris sur l’icône. </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainShortcuts" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Barre des raccourcis -->
<div class="modal hide fade tutorial" id="explainShortcuts" tabindex="-1" role="dialog" aria-labelledby="explainShortcuts" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/sidebar.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">La barre des raccourcis</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>Des raccourcis rapides vers des fonctionnalités importantes du service sont disponibles en cliquant sur la flèche située en haut à droite de l’écran. </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainRH" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- RH -->
<div class="modal hide fade tutorial" id="explainRH" tabindex="-1" role="dialog" aria-labelledby="explainRH" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/rh.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'espace Ressources Humaines</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>La section <i>Ressources Humaines</i> vous permet d’accéder aux offres d’emplois internes, de consulter et de vous inscrire aux formations et enfin de consulter les mouvements du personnel. </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainAlbums" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Les albums -->
<div class="modal hide fade tutorial" id="explainAlbums" tabindex="-1" role="dialog" aria-labelledby="explainAlbums" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/album.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'espace Galerie</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>Les <i>albums</i> regroupent les photos et vidéos pris lors des événements associatifs ou des établissements et services. Vous avez la possibilité d’ajouter votre contenu et de le rendre disponible à tous. </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainChat" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Espace Chat -->
<div class="modal hide fade tutorial" id="explainChat" tabindex="-1" role="dialog" aria-labelledby="explainChat" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/chat.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'espace Chat</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>Le <i>chat</i> vous permet de discuter sur des thématiques professionnelles avec d’autres personnes : membres du service, groupe de projet, etc … Vous pouvez à tout moment demander la création d’un chat pour un besoin professionnel.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainTools" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Espace Outils -->
<div class="modal hide fade tutorial" id="explainTools" tabindex="-1" role="dialog" aria-labelledby="explainTools" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/tools.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'espace Outils</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>La section <i>Outils</i> regroupe l’ensemble des contenus téléchargeables : documentations et ressources. Vous pouvez aussi retrouver le calendrier des événements associatifs.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainNews" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Espace Actualité -->
<div class="modal hide fade tutorial" id="explainNews" tabindex="-1" role="dialog" aria-labelledby="explainNews" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/news.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'espace Actualité</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>La partie <i>Actualité</i> regroupe l’ensemble des actualités et informations internes. Vous avez entre autre accès aux archives du journal associatif mais aussi aux revues de presse.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainSyndicat" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Espace Syndicat -->
<div class="modal hide fade tutorial" id="explainSyndicat" tabindex="-1" role="dialog" aria-labelledby="explainSyndicat" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/syndicat.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'espace Syndicat</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>L’espace <i>Syndicat</i> regroupe les différentes newsletters diffusées par : CFDT, CGT et Sud.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainCCE" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Espace CCE -->
<div class="modal hide fade tutorial" id="explainCCE" tabindex="-1" role="dialog" aria-labelledby="explainCCE" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/cce.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'espace CCE</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>Vous y retrouverez les avantages et services auxquels vous avez droit, des informations sur le CCE ainsi que les procès verbaux.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#explainAccount" data-dismiss="modal">Suivant</button>
            </div>
        </div>
    </div>
</div>
<!-- Espace CCE -->
<div class="modal hide fade tutorial" id="explainAccount" tabindex="-1" role="dialog" aria-labelledby="explainAccount" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-video">
                <video loop autoplay controls>
                    <source src="../extranet/wp-content/uploads/tutorial/account.mp4" type="video/mp4" />
                </video>
            </div>
            <div class="modal-header">
                <h2 class="modal-title">L'espace Mon Compte</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row justify-content-between">
                <p>Modifiez depuis cet espace les informations liées à votre compte, tel que votre mot de passe.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Commencer à utiliser l'extranet !</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#bienvenue1').modal('show');
    });
</script>